package autorization;

import android.util.Base64;

/**
 * Abstract for authentication base
 * Please extend the class to override custom authorization
 * Created by potingchiang on 2016-11-09.
 */

public class Authorization implements IAuthorization {

    //basic variables
    private String userName;
    private String password;
    //constructor
    public Authorization(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    //getter & setter
    @Override
    public String getUserName() {
        return userName;
    }
    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }
    @Override
    public String getPassword() {
        return password;
    }
    @Override
    public void setPassword(String password) {
        this.password = password;
    }
}
