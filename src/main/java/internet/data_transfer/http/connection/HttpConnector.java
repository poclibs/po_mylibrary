package internet.data_transfer.http.connection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import internet.data_transfer.http.configuration.WebConfiguration;
import internet.data_transfer.http.configuration.WebConfigurator;

/**
 * This class is to init and setup basic http connection and configuration
 * Created by potingchiang on 2016-06-29.
 */

public class HttpConnector implements IHttpConnection<WebConfiguration, WebConfigurator> {

    // properties
    private HttpURLConnection       mConn;
    private WebConfiguration        mWebConfig;
    private WebConfigurator         mWebConfigurator;

    // constructor
    public HttpConnector(WebConfiguration webConfig) {
        mWebConfig = webConfig;
        // init web configurator
        mWebConfigurator = new WebConfigurator(mWebConfig);
        // setup/init http connection & configuration
        setupUrlConnection();
    }
    // get https url connection - using singleton
    public HttpURLConnection getHttpUrlConnection() {

        return mConn;
    }
    // setup/init http connection & configuration
    @Override
    public void setupUrlConnection() {
        // init. url and connection
        try {
            mConn = (HttpURLConnection) new URL(mWebConfig.getUrl()).openConnection();
            mWebConfigurator.setupHttpUrlConnection(mConn);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("===\nhttp setup url connection I/O exception: " + e.getMessage() + "\n===");
        }
    }

    @Override
    public WebConfiguration getWebConfig() {
        return mWebConfig;
    }

    @Override
    public WebConfigurator getWebConfigurator() {
        return mWebConfigurator;
    }


    @Override
    public DataOutputStream getDataOutputStream() {

        return mWebConfigurator.getDataOutputStream(mConn);
    }

    @Override
    public BufferedReader getBufferedReader() {

        return mWebConfigurator.getBufferedReader(mConn);
    }

    @Override
    public InputStream getConnInputStream() {

        return mWebConfigurator.getConnInputStream(mConn);
    }

    @Override
    public String getHTTpStatusCodeAndErrorMessage() {

        return mWebConfigurator.getConnResponseCodeAndErrorMessage(mConn);
    }

    @Override
    public String getHTTpStatusCode() {

        return mWebConfigurator.getConnResponseCode(mConn);
    }

    @Override
    public String getConnErrorMessage() {

        return mWebConfigurator.getConnErrorMessage(mConn);
    }

    @Override
    public void processOutputStream(String outputData) {

        try {

//            connection_json.getConn().setDoOutput(false);
            //init data output stream
            if (getDataOutputStream() != null) {

                if (outputData != null) {


                    //write data into data output stream
                    getDataOutputStream().writeBytes(outputData);

                    //dump request data
//                    System.out.println("===\nData output Json tring: " + order_Json_Str + "\n===");
                } else {

                    //error message
                    System.out.println("===\n" + "Output data is null!" + "\n===");
                }

            } else {

                //error message
                System.out.println("===\n" + "DataOutStream is null!" + "\n===");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("===\n" + "ProcessOutputStream error: " + e.getMessage() + "\n===");
        }
    }

    @Override
    public String processInputStream() {

        String serverResult = "N/A";
        try {

            //get result from server in the input stream
            String line;
            if (getBufferedReader() != null) {

                while ((line = getBufferedReader().readLine()) != null) {


                    serverResult += line +"\n";
                }
            }
            else {

                serverResult = "-1";
            }

            //info from server
            //dump request data
            System.out.println("===\nServer result: " + serverResult + "\n===");

        } catch (IOException e) {

            serverResult = e.getMessage() ;
            e.printStackTrace();
            System.out.println("===\n" + "ProcessInputStream error: " + e.getMessage() + "\n===");
        }

        return serverResult;
    }

    //other methods
    @Override
    public void close() {

        //flush & close data output stream
        try {

            if (getDataOutputStream() != null) {

                getDataOutputStream().flush();
                getDataOutputStream().close();
            }
            if (getBufferedReader() != null) {

                getBufferedReader().close();
            }

        } catch (IOException e) {

            //message
            e.printStackTrace();
            System.out.println("===\ndata out put & buffer reader exception: " + e.getMessage() + "\n===");
        }

    }

}
