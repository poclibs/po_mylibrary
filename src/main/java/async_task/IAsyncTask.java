package async_task;

/**
 * This interface is for android async task interaction
 * Created by potingchiang on 2017-03-23.
 */

public interface IAsyncTask {

    void preAsyncTask();
    void doAsyncTask();

}
