package popup_window;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.PopupWindow;

/**
 * interface for popup window base
 * Created by potingchiang on 2017-11-05.
 */

public interface PopupWindowBase {

    // show popup window
    void showPopupAsDropDown(int x, int y);
    void showPopupAsDropDownWithDismissByOutside(int x, int y, int color);
    void showPopupInCenter(int x, int y);
    void showPopupInCenterWithDismissByOutside(int x, int y, int color);
    // dismissPopup
    void dismissPopup();
    // getter & setter
    Context getContext();
    View getParentView();
    int getPopupLayout();
    PopupWindow getPopupWindow();
    void setPopupAnimationStyle(int animationStyle);
    void setPopupBackgroundDrawable(Drawable background);
}
