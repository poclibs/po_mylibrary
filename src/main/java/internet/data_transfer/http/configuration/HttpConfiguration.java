package internet.data_transfer.http.configuration;

import java.net.HttpURLConnection;

/**
 * Interface for basic http configuration
 * Created by potingchiang on 2017-11-15.
 */

public abstract class HttpConfiguration extends Configuration {

    public abstract void setHttpConnectionRequest(HttpURLConnection conn, WebConfiguration config);
    public abstract String getConnResponseCodeAndErrorMessage(HttpURLConnection conn);
    public abstract String getConnResponseCode(HttpURLConnection conn);
    public abstract String getConnErrorMessage(HttpURLConnection conn);

}
