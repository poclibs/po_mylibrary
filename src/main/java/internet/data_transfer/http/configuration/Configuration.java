package internet.data_transfer.http.configuration;

import java.net.URLConnection;

/**
 * Abstract class for basic url connection configuration
 * Created by potingchiang on 2017-11-15.
 */

public abstract class Configuration implements IConfiguration<URLConnection> {}
