package internet.data_transfer.http.configuration;

import javax.net.ssl.HttpsURLConnection;

/**
 * Interface for https connection configuration
 * Created by potingchiang on 2017-11-15.
 */

public abstract class HttpsConfiguration extends HttpConfiguration {

    public abstract void setHttpsConnectionRequest(HttpsURLConnection conn, WebConfiguration config);
    public abstract String getBasicSslAuthorization(String account, String password);
}
