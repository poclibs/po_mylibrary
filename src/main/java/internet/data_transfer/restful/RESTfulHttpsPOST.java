package internet.data_transfer.restful;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONObject;

/** This class is for restful POST basic setup
 * Created by potingchiang on 2016-07-26.
 */
public abstract class RESTfulHttpsPOST extends AsyncTask<String, Void, String> {

    // basic variables
    private AppCompatActivity appCompatActivity;
    private JSONObject request_JSon;
    private ProgressDialog progressDialog;
    private String httpStatusCode;
    private String connErrorMessage;
    private boolean isWriteLogFiles;
    // init file name
    public final static String FILENAME_POSTLOG = "pos_output_log";

    //constructor
    public RESTfulHttpsPOST(AppCompatActivity appCompatActivity, boolean isWriteLogFiles) {
        this.appCompatActivity = appCompatActivity;
        this.isWriteLogFiles = isWriteLogFiles;
    }

    @Override
    protected void onPreExecute() {

        //init progress dialog
        progressDialog = new ProgressDialog(appCompatActivity);

        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Processing...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

        progressDialog.show();

        //init request json
        this.request_JSon = getRequest_JSon();

        //dump request data
        System.out.println("\n===\nData Json: \n" + request_JSon.toString() + "\n===");
        // write log files into internal storage
//        if (isWriteLogFiles) {
//
//            // write or append file into the device
//            new SavingInternalStorage(context, FILENAME_POSTLOG).openFileWrite(request_JSon.toString());
//        }
    }

    @Override
    protected String doInBackground(String... params) {

        //get url string
        String url = params[0];
        // change jSon object to string
        //do request and response
        //init post to server
        WebHttpsJSonCommunicator webHttpsJSonCommunicator
                = new WebHttpsJSonCommunicator(url, "1", "1");
        // do post
        webHttpsJSonCommunicator.doHTTpPOST(request_JSon != null? request_JSon.toString():"N/A");
        //do post, get response and  get result
        String result_res = webHttpsJSonCommunicator.getServerResult();
        //setup status code
        httpStatusCode = webHttpsJSonCommunicator.getHTTpStatusCode();
        connErrorMessage = webHttpsJSonCommunicator.getConnErrorMessage();
        //error message
        System.out.println("===\nConnection error message: " + connErrorMessage + "\n===");
        //return result
        return result_res;
    }

    //override method
    //get request json
    public abstract JSONObject getRequest_JSon();

    //getter & setter
    public AppCompatActivity getAppCompatActivity() {
        return appCompatActivity;
    }

    public void setAppCompatActivity(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

//    public JSONObject getRequest_JSon() {
//        return request_JSon;
//    }
//
//    public void setRequest_JSon(JSONObject request_JSon) {
//        this.request_JSon = request_JSon;
//    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    //get response error message
    public String getConnectionErrorMessage() {

        return connErrorMessage;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }
}
