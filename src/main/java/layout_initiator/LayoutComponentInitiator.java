package layout_initiator;

/**
 * Interface for layout to initiate its components
 * Created by potingchiang on 2017-11-06.
 */

public interface LayoutComponentInitiator {

    void initLayoutComponents();
}
