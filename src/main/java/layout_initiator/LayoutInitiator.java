package layout_initiator;

/**
 * Interface for layout components click listener
 * Created by potingchiang on 2017-11-07.
 */

public interface LayoutInitiator extends LayoutComponentInitiator {

    void initClickListeners();
}
