package internet.data_transfer.restful;

import internet.data_transfer.http.configuration.WebConfiguration;

/**
 * This class is to communicate with server
 * Created by potingchiang on 2016-06-29.
 */
public abstract class WebCommunicator implements IREST {

    // properties
    private final WebConfiguration mWebConfiguration;
    // methods
    public WebCommunicator(String url) {
        mWebConfiguration = new WebConfiguration();
        mWebConfiguration.setUrl(url);
    }

    public void setupPostWithJSon() {

        mWebConfiguration.setupPost();
        mWebConfiguration.setupJSon();
    }
    public void setupGetWithJSon() {

        mWebConfiguration.setupGet();
        mWebConfiguration.setupJSon();
    }
    public void setupSsl(String sslAccount, String sslPassword) {

        mWebConfiguration.setupSsl(sslAccount, sslPassword);
    }
    public WebConfiguration getWebConfiguration() {
        return mWebConfiguration;
    }

}
