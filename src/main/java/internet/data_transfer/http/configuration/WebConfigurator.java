package internet.data_transfer.http.configuration;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import autorization.Base64Authorization;

/**
 * Class for web configuration setup for url, http, https, jason....
 * Created by potingchiang on 2017-11-16.
 */

public class WebConfigurator extends HttpsConfiguration implements IWebConfigurator {

    private final WebConfiguration config;

    public WebConfigurator(WebConfiguration config) {
        this.config = config;
    }

    @Override
    public void setupHttpUrlConnection(HttpURLConnection httpConn) {

        setBasicConnectionRequest(httpConn, config);
        setHttpConnectionRequest(httpConn, config);
    }

    @Override
    public void setupHttpsUrlConnection(HttpsURLConnection httpsConn) {

        setupHttpUrlConnection(httpsConn);
        setHttpsConnectionRequest(httpsConn, config);
    }

    // basic/url configuration
    @Override
    public void setBasicConnectionRequest(URLConnection conn, WebConfiguration config) {

        //setup configuration
        conn.setDoOutput(config.isDoOutput());
        conn.setDoInput(config.isDoInput());
        conn.setUseCaches(config.isUseCaches());
        // using constant for name
        conn.setRequestProperty(config.getConnectionKey(),  config.getConnectionType());
        conn.setRequestProperty(config.getAcceptKey(),      config.getAcceptType());
        conn.setRequestProperty(config.getContentTypeKey(), config.getContentType());
    }

    @Override
    public DataOutputStream getDataOutputStream(URLConnection conn) {
        try {

            //init data output stream
            return new DataOutputStream(conn.getOutputStream());
        } catch (IOException e) {

            //error messages
            e.printStackTrace();
            System.out.print("===\nerror:" + e.getMessage() + "\n==");

            return null;
        }
    }

    @Override
    public BufferedReader getBufferedReader(URLConnection conn) {

        try {

            //init input stream within buffer reader
            return new BufferedReader(new InputStreamReader(conn.getInputStream()));

        } catch (IOException e) {

            //error messages
            e.printStackTrace();
            System.out.print("===\nerror:" + e.getMessage() + "\n==");

            return null;
        }
    }

    @Override
    public InputStream getConnInputStream(URLConnection conn) {

        try {
            return conn.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void setHttpConnectionRequest(HttpURLConnection conn, WebConfiguration config) {

        try {
            conn.setRequestMethod(config.getRequestMethod());
        } catch (ProtocolException e) {
            e.printStackTrace();
            System.out.println("===\nhttp setup request method exception: " + e.getMessage() + "\n===");
        }
    }

    @Override
    public String getConnResponseCodeAndErrorMessage(HttpURLConnection conn) {

        try {
            return Integer.toString( conn.getResponseCode()) + "\n" + conn.getResponseMessage();
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @Override
    public String getConnResponseCode(HttpURLConnection conn) {

        try {
            return Integer.toString(conn.getResponseCode());
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @Override
    public String getConnErrorMessage(HttpURLConnection conn) {

        //init error message
        String eMessage =  "No Error.";
        try {

            if (conn.getResponseMessage() != null) {

                eMessage = conn.getResponseMessage();
            }

        } catch (IOException e) {

            //message
            e.printStackTrace();
            eMessage = e.getMessage();
        }

        return eMessage;
    }

    @Override
    public void setHttpsConnectionRequest(HttpsURLConnection conn, WebConfiguration config) {

        conn.setRequestProperty(
                config.getAuthorizationKey(),
                getBasicSslAuthorization(config.getSslAccount(), config.getSslPassword()));
    }

    @Override
    public String getBasicSslAuthorization(String account, String password) {

        return "Basic " + new Base64Authorization(account, password).getEncodedAuthorization();
    }
}
