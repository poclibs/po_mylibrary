package internet.data_transfer.restful;

import internet.data_transfer.http.connection.HttpConnector;
import internet.data_transfer.http.connection.HttpsConnector;

/**
 * This class is to communicate with server through http connection
 * Created by potingchiang on 2017-11-23.
 */

public class WebHttpsJSonCommunicator extends WebCommunicator {

    // properties
    private final HttpsConnector mHttpsConnector;
    private String serverResult;

    public WebHttpsJSonCommunicator(String url, String sslAccount, String sslPassword) {
        super(url);
        // setup ssl
        setupSsl(sslAccount, sslPassword);
        mHttpsConnector = new HttpsConnector(getWebConfiguration());
    }

    @Override
    public void doHTTpPOST(String outputData) {

        // setup json and post
        setupPostWithJSon();
        // do post
        mHttpsConnector.processOutputStream(outputData);
        // get result
        serverResult = mHttpsConnector.processInputStream();
    }

    @Override
    public void doHTTpGET() {

        // setup json and get
        setupGetWithJSon();
        serverResult = mHttpsConnector.processInputStream();
    }

    @Override
    public void doHTTpHEAD() {

    }

    @Override
    public void doHTTpOPTIONS() {

    }

    @Override
    public void doHTTpPUT() {

    }

    @Override
    public void doHTTpDELETE() {

    }

    @Override
    public void doHTTpTRACE() {

    }

    @Override
    public String getHTTpStatusCode() {
        return mHttpsConnector.getHTTpStatusCode();
    }

    @Override
    public String getServerResult() {
        return serverResult;
    }

    @Override
    public String getConnErrorMessage() {
        return mHttpsConnector.getConnErrorMessage();
    }

    @Override
    public String getHTTpStatusCodeAndErrorMessage() {
        return mHttpsConnector.getHTTpStatusCodeAndErrorMessage();
    }
}
