package internet.data_transfer.http.connection;

import java.io.IOException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import internet.data_transfer.http.configuration.WebConfiguration;

/**
 * This class is to init and setup basic http connection and configuration
 * Created by potingchiang on 2016-06-29.
 */

public class HttpsConnector extends HttpConnector {

    // properties
    private HttpsURLConnection   mConn;
    // constructor
    public HttpsConnector(WebConfiguration webConfig) {
        super(webConfig);
    }
    // get https url connection - using singleton
    public HttpsURLConnection getHttpsUrlConnection() {

        return mConn;
    }
    // override setup url connection without super.setupUrlConnection()
    @Override
    public void setupUrlConnection() {
        // init. url and connection
        try {
            mConn = (HttpsURLConnection) new URL(getWebConfig().getUrl()).openConnection();
            getWebConfigurator().setupHttpsUrlConnection(mConn);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("===\nhttps setup url connection I/O exception: " + e.getMessage() + "\n===");
        }
    }
}
