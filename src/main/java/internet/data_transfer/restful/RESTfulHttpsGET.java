package internet.data_transfer.restful;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;


/**
 * This class is for restful GET basic setup
 * Created by potingchiang on 2016-07-26.
 */
public class RESTfulHttpsGET extends AsyncTask<String, Void, String> {

    //basic variables
//    private Context context;
    private AppCompatActivity appCompatActivity;
    private ProgressDialog progressDialog;
    private String httpStatusCode;

    //constructor
    public RESTfulHttpsGET(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    //override method
    @Override
    protected void onPreExecute() {
//        super.onPreExecute();

        //init progress dialog
        progressDialog = new ProgressDialog(appCompatActivity);

        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Processing...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

        if (!progressDialog.isShowing()) {

            progressDialog.show();
        }

    }

    @Override
    protected String doInBackground(String... params) {

        //setup url
        String url = params[0];
        //init talk to server with GET
        WebHttpsJSonCommunicator webHttpsJSonCommunicator
                = new WebHttpsJSonCommunicator(url, "1", "1");
        // do get
        webHttpsJSonCommunicator.doHTTpGET();
        //get result from server
        String result = webHttpsJSonCommunicator.getServerResult();
        //get status code
        httpStatusCode = webHttpsJSonCommunicator.getHTTpStatusCode();
        //return result
        return result;
    }

    //getter & setter
    public AppCompatActivity getAppCompatActivity() {
        return appCompatActivity;
    }

    public void setAppCompatActivity(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }
}
