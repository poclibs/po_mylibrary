package internet.data_transfer.image_transfer;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

/**
 * This class is for restful GET image basic setup
 * Created by potingchiang on 2016-07-26.
 */
public class GET_IMG_FromURL_Drawable extends AsyncTask<String, Void, Drawable> {

    //basic variables
//    private Context context;
    private AppCompatActivity appCompatActivity;
    private ProgressDialog progressDialog;

    //constructor
    public GET_IMG_FromURL_Drawable(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    //override method
    @Override
    protected void onPreExecute() {
//        super.onPreExecute();

        //init progress dialog
//        progressDialog = ProgressDialog.show(
//                context,
//                "Please wait", "Processing...",
//                true,
//                true
//        );
    }

    @Override
    protected Drawable doInBackground(String... params) {

        //setup url
        String url = params[0];
        //init talk to server with GET
        Url_Handler url_handler = new Url_Handler(url);
        //return result
        return  url_handler.getImgFromUrl_Drawable("dl_Img");
    }

    //getter & setter
    public AppCompatActivity getAppCompatActivity() {
        return appCompatActivity;
    }

    public void setAppCompatActivity(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }
}
