package autorization;

/**
 * Interface for authorization
 * Created by potingchiang on 2017-11-14.
 */

public interface IAuthorization {

    //getter & setter
    String getUserName();
    void setUserName(String userName);
    String getPassword();
    void setPassword(String password);

}
