package internet.data_transfer.http.connection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * interface for http/https connection and configuration
 * Created by potingchiang on 2016-06-29.
 *
 * ----------------------------
 * POS server status code list:
 * GET
 * ok -> 200
 * NG ->
 * ----------------------------
 * POST/Create:
 * ok -> 201
 * NG -> 422
 * ----------------------------
 * PUT/Update:
 * ok -> 200
 * NG -> 422
 * ----------------------------
 * DELETE:
 * ok -> 200
 * NG ->
 *----------------------------
 *
 */

public interface IHttpConnection<Object, Configurator> {

    void setupUrlConnection();

    /**
     * @return web configuration or any object extend from it
     */
    Object getWebConfig();
    Configurator getWebConfigurator();
    DataOutputStream getDataOutputStream();
    BufferedReader getBufferedReader();
    //get input stream
    InputStream getConnInputStream();
    //get status code & error message
    String getHTTpStatusCodeAndErrorMessage();
    //get status code
    String getHTTpStatusCode();
    //get error message
    String getConnErrorMessage();
    // process output & input
    void processOutputStream(String outputData);
    String processInputStream();

    /**
     * close() is to close DataOutputStream and BufferReader
     * code your own overriding this method
     */
    void close();

}
