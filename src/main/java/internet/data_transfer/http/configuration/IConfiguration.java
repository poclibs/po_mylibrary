package internet.data_transfer.http.configuration;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;

/**
 * Baisc interface for http/https configuration
 * Created by potingchiang on 2017-11-15.
 */

public interface IConfiguration<C> {

    // setup property
    void setBasicConnectionRequest(C conn, WebConfiguration config);
    DataOutputStream getDataOutputStream(C conn);
    BufferedReader getBufferedReader(C conn);
    InputStream getConnInputStream(C conn);
}
