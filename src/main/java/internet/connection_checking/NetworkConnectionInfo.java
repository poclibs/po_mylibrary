package internet.connection_checking;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * This class is to check if network is connected
 * Created by potingchiang on 2016-06-22.
 */
public class NetworkConnectionInfo {

    private Context context;
    private boolean isConnected;
    private NetworkInfo networkInfo;

    public NetworkConnectionInfo(Context context) {
        this.context = context;
    }

    public boolean isConnected() {

        isConnected = false;
        //init connection manager/mgr
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(
                Activity.CONNECTIVITY_SERVICE
        );

        //get network info
        // before using, add "android.permission.ACCESS_NETWORK_STATE"
        // to permission in manifest.xml
        if (connMgr != null) {
            networkInfo = connMgr.getActiveNetworkInfo();
            isConnected = true;
        }

        //check network connection
        if (networkInfo != null && networkInfo.isConnected()) {

            isConnected = true;
        }
        else {

            isConnected = false;
        }

        return isConnected;
    }

    //getter & setter
    public NetworkInfo getNetworkInfo() {
        return networkInfo;
    }
}
