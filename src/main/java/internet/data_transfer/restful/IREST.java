package internet.data_transfer.restful;

/**
 * Basic interface for web restul communication
 * Created by potingchiang on 2017-11-19.
 */

public interface IREST {

    // do HTTP connection with through REST
    void doHTTpPOST(String outputData);
    void doHTTpGET();
    void doHTTpHEAD();
    void doHTTpOPTIONS();
    void doHTTpPUT();
    void doHTTpDELETE();
    void doHTTpTRACE();
    /**
     * get HTTP status code from server
     * @return String - HTTP status code
     */
    String getHTTpStatusCode();
    /**
     * get result code from server
     * @return String - result
     */
    String getServerResult();
    String getConnErrorMessage();
    String getHTTpStatusCodeAndErrorMessage();
}
