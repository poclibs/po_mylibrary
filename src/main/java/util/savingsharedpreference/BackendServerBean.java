package util.savingsharedpreference;

import java.io.Serializable;
import java.util.List;

/**
 * java bean for backend server shared preference
 * Created by potingchiang on 2017-08-05.
 */

public class BackendServerBean implements Serializable {

    // domain name
    private String domainName;
    // domain url
    private String domainUrl;
    // mid name
    private String webMid;

    public String getWebMid() {
        return webMid;
    }

    public void setWebMid(String webMid) {
        this.webMid = webMid;
    }

    // version list
    private List<String> versions;
    // account list
    private List<String> accounts;
    // password list
    private List<String> passwords;

    // getter & setter
    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getDomainUrl() {
        return domainUrl;
    }

    public void setDomainUrl(String domainUrl) {
        this.domainUrl = domainUrl;
    }

    public List<String> getVersions() {
        return versions;
    }

    public void setVersions(List<String> versions) {
        this.versions = versions;
    }

    public List<String> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<String> accounts) {
        this.accounts = accounts;
    }

    public List<String> getPasswords() {
        return passwords;
    }

    public void setPasswords(List<String> passwords) {
        this.passwords = passwords;
    }
}
