package autorization;

import android.util.Base64;

/**
 * Authentication for Base64
 * Created by potingchiang on 2016-11-09.
 */

public class Base64Authorization extends Authorization {

    // properties
    private String authorization;
    //constructor
    public Base64Authorization(String userName, String password) {
        super(userName, password);
        authorization = userName + ":" + password;
    }
    // methods
    // get basic base64 encode
    public String getEncodedAuthorization() {

        //return encoded authentication
        return Base64.encodeToString(this.authorization.getBytes(), Base64.DEFAULT);
    }
}
