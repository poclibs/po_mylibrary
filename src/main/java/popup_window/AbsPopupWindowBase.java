package popup_window;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

/**
 * This class is to the base to initiate popup window
 * Created by potingchiang on 2015-11-09.
 */
public abstract class AbsPopupWindowBase implements PopupWindowBase {

    private final Context context;
    private final View view;
    private final int layout;
    private final PopupWindow popupWindow;

    //constructor
    public AbsPopupWindowBase(Context context, int layout, ViewGroup root,
                              boolean attachToRoot) {
        this.context = context;
        this.layout = layout;

        // init. view
        view = LayoutInflater.from(context).inflate(layout, root, attachToRoot);
        // init popup window
        popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT, true);
    }

    @Override
    public void showPopupAsDropDown(int x, int y) {

        //popup window default location
        popupWindow.showAsDropDown(view, x, y);

    }

    @Override
    public void showPopupAsDropDownWithDismissByOutside(int x, int y, int color) {

        //popup window default location
        popupWindow.showAsDropDown(view, x, y);

    }

    @Override
    public void showPopupInCenter(int x, int y) {

        //popup window default location
        popupWindow.showAtLocation(view, Gravity.CENTER, x, y);

    }

    @Override
    public void showPopupInCenterWithDismissByOutside(int x, int y, int color) {

        //popup window shows up
        //popup window default location
        popupWindow.showAtLocation(view, Gravity.CENTER, x, y);

    }

    @Override
    public void dismissPopup() {

        popupWindow.dismiss();
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public View getParentView() {

        return view;
    }

    @Override
    public int getPopupLayout() {

        return layout;
    }

    @Override
    public PopupWindow getPopupWindow() {

        return popupWindow;
    }

    @Override
    public void setPopupAnimationStyle(int animationStyle) {

        this.popupWindow.setAnimationStyle(animationStyle);
    }

    @Override
    public void setPopupBackgroundDrawable(Drawable background) {

        this.popupWindow.setBackgroundDrawable(background);
    }

}
