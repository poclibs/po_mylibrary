package util.savingsharedpreference;

import android.content.Context;

import java.util.List;

/**
 * The interface for backed server shared preference
 * Created by potingchiang on 2017-08-04.
 */

public interface iBackendServerSharedPreference {

    /**
     * save domain names
     * @param context       context for the current shared preference
     * @param mode          sharePreference save mode
     * @param domainName    domain name must equal to key name
     * @return true if it is saved in sharedPreference successfully
     */
    boolean saveDomainName(Context context, int mode, String domainName);

    /**
     * get domain key/name
     * @param context       context for the current shared preference
     * @param domainName    domain name from sharedPreference as the key for domain info
     * @param mode          sharePreference save mode
     * @return domain key/name for domain info shared preference to use
     */
    String getDomainName(Context context, String domainName, int mode);

    /**
     * get domain key/name
     * @param context       context for the current shared preference
     * @param domainName    domain name from sharedPreference as the key for domain info
     * @param mode          sharePreference save mode
     * @return domain key/name list for domain info shared preference to use
     */
    List<String> getDomainNames(Context context, String domainName, int mode);

    /**
     * save server information
     * @param context       context for the current shared preference
     * @param mode          sharePreference save mode
     * @param domainKey     domain key/name from "getDomainKey"
     * @param serverInfo    (BackendServeBean for server information
     * @return true if it is saved in sharedPreference successfully
     */
    boolean saveServerInfo(Context context, String domainKey, int mode, BackendServerBean serverInfo);

    /**
     * get server information
     * @param context       context for the current shared preference
     * @param domainKey get certain server information from shared preference
     * @param mode          sharePreference save mode
     * @return backend server bean with certain server information
     */
    BackendServerBean getServerInfo(Context context, String domainKey, int mode);

}
