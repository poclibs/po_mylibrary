package alert_window;

/**
 * This interface is for alert popup window interface
 * Created by potingchiang on 2017-03-24.
 */

public interface IAlert {

    void initAlertPopup();
}
