package internet.data_transfer.restful;

import internet.data_transfer.http.connection.HttpConnector;

/**
 * This class is to communicate with server through http connection
 * Created by potingchiang on 2017-11-23.
 */

public class WebHttpJSonCommunicator extends WebCommunicator {

    // properties
    private final HttpConnector mHttpConnector;
    private String serverResult;

    public WebHttpJSonCommunicator(String url) {
        super(url);
        mHttpConnector = new HttpConnector(getWebConfiguration());
    }

    @Override
    public void doHTTpPOST(String outputData) {

        // setup json and post
        setupPostWithJSon();
        // do post
        mHttpConnector.processOutputStream(outputData);
        // get result
        serverResult = mHttpConnector.processInputStream();
    }

    @Override
    public void doHTTpGET() {

        // setup json and get
        setupGetWithJSon();
        serverResult = mHttpConnector.processInputStream();
    }

    @Override
    public void doHTTpHEAD() {

    }

    @Override
    public void doHTTpOPTIONS() {

    }

    @Override
    public void doHTTpPUT() {

    }

    @Override
    public void doHTTpDELETE() {

    }

    @Override
    public void doHTTpTRACE() {

    }

    @Override
    public String getHTTpStatusCode() {
        return mHttpConnector.getHTTpStatusCode();
    }

    @Override
    public String getServerResult() {
        return serverResult;
    }

    @Override
    public String getConnErrorMessage() {
        return mHttpConnector.getConnErrorMessage();
    }

    @Override
    public String getHTTpStatusCodeAndErrorMessage() {
        return mHttpConnector.getHTTpStatusCodeAndErrorMessage();
    }
}
