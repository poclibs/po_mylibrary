package internet.data_transfer.http.configuration;

import java.net.HttpURLConnection;

import javax.net.ssl.HttpsURLConnection;

/**
 * Interface for developers to interact with WebConfigurator
 * Created by potingchiang on 2017-11-16.
 */

public interface IWebConfigurator {

    // http/https connection
    void setupHttpUrlConnection(HttpURLConnection httpConn);
    void setupHttpsUrlConnection(HttpsURLConnection httpsConn);
}
