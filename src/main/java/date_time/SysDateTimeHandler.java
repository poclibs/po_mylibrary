package date_time;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This class is to generate system date
 * Created by potingchiang on 2016-02-02.
 */
public class SysDateTimeHandler {

    private SimpleDateFormat sysDate;
    private SimpleDateFormat sysDateTime;
    private Locale locale;
    private String date_Format;
    private String dateTime_Format;

    public SysDateTimeHandler() {

        this.locale = Locale.CANADA;
    }
    public SysDateTimeHandler(Locale locale) {

        this.locale = locale;
    }
    public String getSysDate() {

        date_Format = "MMM-dd-yyyy";
        sysDate = new SimpleDateFormat(date_Format, locale);

        return sysDate.format(new Date());
    }
    public String getSysDateTime() {

        dateTime_Format = "MMM-dd-yyyy HH:mm";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
    public String getSysTime() {

        dateTime_Format = "HH:mm:ss";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
    public String getSysTimeHour() {

        dateTime_Format = "hh";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
    public String getSysTimeMin() {

        dateTime_Format = "mm";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
    public String getSysTimeSec() {

        dateTime_Format = "ss";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
    public String getSysAmPm() {

        dateTime_Format = "a";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
}
