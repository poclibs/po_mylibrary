package async_task;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import alert_window.IAlert;
import internet.connection_checking.NetworkConnectionInfo;
import httpexample.potingchiang.com.mylibrary.R;

/**
 * This is rest factory for the restful
 * Created by potingchiang on 2017-03-23.
 */

public abstract class AsyncFactory implements IAsyncTask, IAlert {

    // variables
    private final Context mContext;
    // constructor
    public AsyncFactory(Context context) {
        mContext = context;
    }
    // override
    @Override
    public void preAsyncTask() {

        // init. environment network information handler
        NetworkConnectionInfo mNetworkInfo = new NetworkConnectionInfo(mContext);
        // check network is working before doing rest
        if (mNetworkInfo.isConnected()) {

            //do async task
            doAsyncTask();
        }
        else {

            // message
            // codes....

            // popup window
            initAlertPopup();
        }
    }
    // init alert window
    @Override
    public void initAlertPopup() {

        //init. dialog
        AlertDialog.Builder myBuilder = new AlertDialog.Builder(mContext);
        //setup dialog info
        myBuilder.setCancelable(false);
        myBuilder.setTitle(mContext.getString(R.string.INTERNETCONNECTIONPROBLMES));
        myBuilder.setMessage(mContext.getString(R.string.CHECKTRYINTERNET));
        myBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //cancel dialog
                dialog.cancel();
                preAsyncTask();
            }
        });
        myBuilder.setNegativeButton("wifi setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                // do something
                // codes...

                // go to setting wifi
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        myBuilder.create().show();
    }
}
