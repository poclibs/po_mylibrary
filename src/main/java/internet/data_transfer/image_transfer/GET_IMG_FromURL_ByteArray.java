package internet.data_transfer.image_transfer;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

/**
 * This class is for restful GET image basic setup
 * Created by potingchiang on 2016-07-26.
 */
public class GET_IMG_FromURL_ByteArray extends AsyncTask<String, Void, byte[]> {

    //basic variables
//    private Context context;
    private AppCompatActivity appCompatActivity;
    private ProgressDialog progressDialog;
    private Bitmap.CompressFormat format;
    private int quality;

    //constructor
    public GET_IMG_FromURL_ByteArray(AppCompatActivity appCompatActivity, Bitmap.CompressFormat format, int quality) {
        this.appCompatActivity = appCompatActivity;
        this.format = format;
        this.quality = quality;
    }

    //override method
    @Override
    protected void onPreExecute() {
//        super.onPreExecute();

//        //init progress dialog
//        progressDialog = ProgressDialog.show(
//                context,
//                "Please wait", "Processing...",
//                true,
//                true
//        );
    }

    @Override
    protected byte[] doInBackground(String... params) {

        //setup url
        String url = params[0];
        //init url handler
        //init talk to server with GET
        Url_Handler url_handler = new Url_Handler(url);
        //return result
        return  url_handler.getImgFromUrl_ByteArray(format, quality);
    }

    //getter & setter
    public AppCompatActivity getAppCompatActivity() {
        return appCompatActivity;
    }

    public void setAppCompatActivity(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }
}
