package internet.data_transfer.http.configuration;

import android.util.ArrayMap;

/**
 * Web configuration for http/https connection properties
 * implementation Parcelable - not done yet
 * Created by potingchiang on 2017-11-15.
 */

public class WebConfiguration {

    // key sets
    // get key name with static method to avoid using too many memories
    private final String CONNECTION_KEY     = "Connection";
    private final String ACCEPT_KEY         = "Accept";
    private final String CONTENT_TYPE_KEY   = "Content-Type";
    private final String AUTHORIZATION_KEY  = "Authorization";

    public String getConnectionKey() {
        return CONNECTION_KEY;
    }
    public String getAcceptKey() {
        return ACCEPT_KEY;
    }

    public String getContentTypeKey() {
        return CONTENT_TYPE_KEY;
    }

    public String getAuthorizationKey() {
        return AUTHORIZATION_KEY;
    }
    // property sets
    // connection type
    private final String CONNECTION_KEEP_ALIVE = "keep-alive";
    public ArrayMap<String, String> getConnectionTypes() {

        ArrayMap<String, String> map = new ArrayMap<>();
        // keep alive
        map.put(CONNECTION_KEEP_ALIVE, "keep-alive");
        // ...more options

        return map;
    }
    // accept type
    private final String ACCEPT_APP_JSON = "application/json";
    public ArrayMap<String, String> getAcceptTypes() {

        ArrayMap<String, String> map = new ArrayMap<>();
        // application/json
        map.put(ACCEPT_APP_JSON, "application/json");
        // ...more options

        return map;
    }
    // content type
    private final String CONTENT_TYPE_APP_JSON      = "application/json";
    private final String CONTENT_TYPE_TEXT_PLAIN    = "text/plain";

    public ArrayMap<String, String> getContentTypes() {

        ArrayMap<String, String> map = new ArrayMap<>();
        // application/json
        map.put(CONTENT_TYPE_APP_JSON, "application/json");
        // text/[;ain
        map.put(CONTENT_TYPE_TEXT_PLAIN, "text/plain");
        // ...more options

        return map;
    }

    // property
    /**
     *
     * url                  url to make to web connection
     * isDoOutput           is to do out put
     * isDoInput            is to do in  put
     * isUseCaches          is to use cashes
     * requestMethod        http supported method
     *                          - "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE"
     * sslAccount           ssl account
     * sslPassword          ssl password
     * connectionType       connection    type, ex, keep-alive
     * acceptType           accept        type, ex: application/json
     * contentType          content       type, ex: application/json or text/plain
     * authorizationType    authorization type, ex: Basic Base64
     */
    private String  url;
    private boolean isDoOutput;
    private boolean isDoInput;
    private boolean isUseCaches;
    private String  requestMethod;
    private String  sslAccount;
    private String  sslPassword;
    private String  connectionType;
    private String  acceptType;
    private String  contentType;
    private String  authorizationType;
    // http/https methods
    private final String GET    = "GET";
    private final String POST   = "POST";
    private final String HEAD   = "HEAD";
    private final String PUT    = "PUT";
    private final String DELETE = "DELETE";
    private final String TRACE  = "TRACE";
    // constructor
    public WebConfiguration() {
    }
    // do post
    public void setupPost() {

        isDoInput       = true;
        isDoOutput      = true;
        isUseCaches     = false;
        requestMethod   = POST;
    }
    // with GET
    public void setupGet() {

        isDoInput       = true;
        isDoOutput      = false;
        isUseCaches     = false;
        requestMethod   = GET;
    }
    // setup basic json
    public void setupJSon() {

        connectionType  = getConnectionTypes().get(CONNECTION_KEEP_ALIVE);
        acceptType      = getAcceptTypes().get(ACCEPT_APP_JSON);
        contentType     = getContentTypes().get(CONTENT_TYPE_APP_JSON);
    }
    public void setupSsl(String sslAccount, String sslPassword) {

        sslAccount  = sslAccount;
        sslPassword = sslPassword;
    }

    // getter & setter
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isDoOutput() {
        return isDoOutput;
    }

    public void setDoOutput(boolean doOutput) {
        isDoOutput = doOutput;
    }

    public boolean isDoInput() {
        return isDoInput;
    }

    public void setDoInput(boolean doInput) {
        isDoInput = doInput;
    }

    public boolean isUseCaches() {
        return isUseCaches;
    }

    public void setUseCaches(boolean useCaches) {
        isUseCaches = useCaches;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getSslAccount() {
        return sslAccount;
    }

    public void setSslAccount(String sslAccount) {
        this.sslAccount = sslAccount;
    }

    public String getSslPassword() {
        return sslPassword;
    }

    public void setSslPassword(String sslPassword) {
        this.sslPassword = sslPassword;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public String getAcceptType() {
        return acceptType;
    }

    public void setAcceptType(String acceptType) {
        this.acceptType = acceptType;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getAuthorizationType() {
        return authorizationType;
    }

    public void setAuthorizationType(String authorizationType) {
        this.authorizationType = authorizationType;
    }
}
