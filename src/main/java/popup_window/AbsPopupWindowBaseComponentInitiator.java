package popup_window;

import android.content.Context;
import android.view.ViewGroup;

import layout_initiator.LayoutComponentInitiator;

/**
 * This class is to the base to initiate popup window
 * Created by potingchiang on 2015-11-09.
 */
public abstract class AbsPopupWindowBaseComponentInitiator extends AbsPopupWindowBase
        implements LayoutComponentInitiator {

    //constructor
    public AbsPopupWindowBaseComponentInitiator(Context context, int layout, ViewGroup root, boolean attachToRoot) {
        super(context, layout, root, attachToRoot);
    }
}
